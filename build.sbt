organization := "xyz.outka"
name := "json-validator"
version := "0.0.1-SNAPSHOT"
scalaVersion := "2.11.8"

val Http4sVersion = "0.15.11a"

libraryDependencies ++= Seq(
 "org.http4s"     %% "http4s-blaze-server" % Http4sVersion,
 "org.http4s"     %% "http4s-circe"        % Http4sVersion,
 "org.http4s"     %% "http4s-dsl"          % Http4sVersion,
 "org.http4s"     %% "http4s-blaze-client" % Http4sVersion,
 "ch.qos.logback" %  "logback-classic"     % "1.2.1",
 "com.github.fge" % "json-schema-validator" % "2.2.6",
 "org.scalaz" %% "scalaz-core" % "7.2.12"
)
