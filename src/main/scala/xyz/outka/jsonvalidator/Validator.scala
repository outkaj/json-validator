package xyz.outka.jsonvalidator

import io.circe._
import org.http4s._
import org.http4s.circe._
import org.http4s.server._
import org.http4s.dsl._
import org.http4s.client.blaze._
import scalaz.concurrent.Task
import com.fasterxml.jackson.databind.JsonNode
import com.github.fge.jsonschema.core.exceptions.ProcessingException
import com.github.fge.jsonschema.core.report.ProcessingReport
import com.github.fge.jsonschema.main.JsonSchema
import com.github.fge.jsonschema.examples.Utils

case class Schema (id: String)

object Validator {
  val httpClient = PooledHttp1Client()
  // Decode the Hello response
  def helloClient(id: String): Task[Schema] = {
  // Encode a User request
  val req = POST(uri("http://localhost:8080/schema"), Id(name).asJson)
  // Decode a Hello response
  httpClient.expect(req)(jsonOf[Schema])
}

def validate(filename: String): ProcessingReport = {
    val configSchema: JsonNode = Utils.loadResource("config-schema.json")
    val good: JsonNode = Utils.loadResource("config.json")
    val bad: JsonNode = Utils.loadResource("config-bad.json")
    val factory: JsonSchemaFactory = JsonSchemaFactory.byDefault()
    val schema: JsonSchema = factory.getJsonSchema(configSchema)
    val report: ProcessingReport = schema.validate(good)
    report
}

  val service = HttpService {
    case GET -> Root / "schema" / id =>
      Ok(Json.obj("message" -> Json.fromString(s"id")))
    case req @ POST -> Root / "schema" / id =>
      for {
        // Decode a request
        schema <- req.as(jsonOf[Schema])
        // Encode a response
        resp <- Ok(Schema(schema.id).asJson)
      } yield (resp)
    case POST -> Root / "validate" / id =>
      validate(s"$id.json")
    }

}
