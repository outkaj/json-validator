Scala REST service for validating JSON documents against JSON schema.

Uses HTTP4s for the REST API, json-schema-validator for JSON schema validation,
and local filesystem for persistent storage.

Stay tuned for running instructions. 
